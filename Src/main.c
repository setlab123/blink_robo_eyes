
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);


void clock(void);
void lat(void);
void oe(void);


uint64_t old_red_bin[1][32]={{0x0, 0x0, 0x1fe00007f8000, 0x1fff0000fff800, 0x7fff8001fffe00, 0x1fe018001807f80, 0x7800000000001e0, 0xe00000000000070, 0x1800000000000018, 0x2000000000000004, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};

uint64_t old_green_bin[1][32]={{0x0, 0x0, 0x1fe00007f8000, 0x1fff0000fff800, 0x7fff8001fffe00, 0x1fe018001807f80, 0x7800000000001e0, 0xe00000000000070, 0x1800000000000018, 0x2000000000000004, 0x0, 0xff000000ff000, 0xfffe00007fff00, 0x1ffff8001ffff80, 0x7ffffc003ffffe0, 0xfffffc003fffff0, 0x1fffffe007fffff8, 0x1fffffe007fffff8, 0x1fffffe007fffff8, 0xfffffe007fffff0, 0xfffffe007fffff0, 0x7ffffe007fffff0, 0x7ffffc003ffffe0, 0x3ffffc003ffffe0, 0x3ffff8001ffffc0, 0x1ffff0000ffff80, 0xfffe00007fff00, 0x3ffc00003ffc00, 0xfe0000007f000, 0x0, 0x0, 0x0, },
};

uint64_t old_blue_bin[1][32]={{0x0, 0x0, 0x1fe00007f8000, 0x1fff0000fff800, 0x7fff8001fffe00, 0x1fe018001807f80, 0x7800000000001e0, 0xe00000000000070, 0x1800000000000018, 0x2000000000000004, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};





uint64_t eye_red_bin[1][10]={{0xfffc3ffffffc3fff, 0xfff00ffffff80fff, 0xfff00ffffff00fff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xfff00ffffff00fff, 0xfff00ffffff00fff, 0xfffc3ffffffc3fff},
};

uint64_t eye_green_bin[1][10]={{0xfffc3ffffffc3fff, 0xfff00ffffff80fff, 0xfff00ffffff00fff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xfff00ffffff00fff, 0xfff00ffffff00fff, 0xfffc3ffffffc3fff},
};

uint64_t eye_blue_bin[1][10]={{0xfffc3ffffffc3fff, 0xfff00ffffff80fff, 0xfff00ffffff00fff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xffe007ffffe007ff, 0xfff00ffffff00fff, 0xfff00ffffff00fff, 0xfffc3ffffffc3fff},
};



uint64_t clouse_1[1][32]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfff007ffffe00fff, 0xff0000ffff0000ff, 0xfe00007ffe00007f, 0xfe00003ffc00007f, 0xff00007ffe0000ff, 0xffc000ffff0003ff, 0xfff007ffffe00fff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff}
};

uint64_t clouse_2[1][32]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfff007ffffe00fff, 0xff0000ffff0000ff, 0xfe00007ffe00007f, 0xfc00003ffc00003f, 0xe000003ffc000007, 0xe000001ff8000007, 0xe000001ff8000007, 0xf000003ffc00000f, 0xf800007ffe00001f, 0xfe0000ffff00007f, 0xff0003ffffc000ff, 0xff801ffffff801ff, 0xffe07ffffffe07ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff}
};

uint64_t clouse_3[1][32]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfff007ffffe00fff, 0xff0000ffff0000ff, 0xfe00007ffe00007f, 0xfc00003ffc00003f, 0xe000003ffc000007, 0xe000001ff8000007, 0xe000001ff8000007, 0xe000001ff8000007, 0xe000001ff8000007, 0xf000001ff800000f, 0xf000001ff800000f, 0xf000001ff800000f, 0xfc00001ff800003f, 0xfc00007ffe00003f, 0xff0000ffff0000ff, 0xff8001ffff8001ff, 0xffe003ffffc007ff, 0xfff80ffffff01fff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};


uint64_t red_bin[1][32]={{}};

uint64_t green_bin[1][32]={{}};

uint64_t blue_bin[1][32]={{}};


int main(void)
{


  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
  char str_tx[25];
  extern uint8_t str_rx[25];
  uint8_t str_us[25];
  extern uint8_t len;
  int X = 0;
  int PANEL_X_MAX=128;
  int Y = 0;


  int x_usb = 32;
  int y_usb = 15;

  int c = 0;
  int c_last = 0;

  int time = 0;
  int last_time = 0;
  int d_time = 0;

  int d_k = 110;
  int d_b = 6000;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {


//	    sprintf(str_tx,"count: [%ld]\n", len);




		memcpy(str_tx, str_rx, sizeof(str_tx));

		time = HAL_GetTick();
		d_time = time - last_time;


	  	if(c == 0 || d_time< d_b){
			memcpy(green_bin, old_green_bin, sizeof(green_bin));
			memcpy(red_bin, old_red_bin, sizeof(red_bin));
			memcpy(blue_bin, old_blue_bin, sizeof(blue_bin));
			c = 1;
	  	}

		if(d_time>d_b &&  c == 1){
			c = 2;
			for(int cc = 0; cc<32; cc++){
				green_bin[0][cc] = green_bin[0][cc] & clouse_1[0][cc];
				red_bin[0][cc] = red_bin[0][cc] & clouse_1[0][cc];
				blue_bin[0][cc] = blue_bin[0][cc] & clouse_1[0][cc];
			}
		}


		if(d_time>d_b + d_k &&  c == 2){
			c = 3;
			for(int cc = 0; cc<32; cc++){
				green_bin[0][cc] = green_bin[0][cc] & clouse_2[0][cc];
				red_bin[0][cc] = red_bin[0][cc] & clouse_2[0][cc];
				blue_bin[0][cc] = blue_bin[0][cc] & clouse_2[0][cc];
			}
		}


		if(d_time>d_b + d_k*2 &&  c == 3){
			c = 4;
			for(int cc = 0; cc<32; cc++){
				green_bin[0][cc] = green_bin[0][cc] & clouse_3[0][cc];
				red_bin[0][cc] = red_bin[0][cc] & clouse_3[0][cc];
				blue_bin[0][cc] = blue_bin[0][cc] & clouse_3[0][cc];
			}
		}

		if(d_time>d_b + d_k*3 &&  c == 4){
			c = 5;
			memcpy(green_bin, old_green_bin, sizeof(green_bin));
			memcpy(red_bin, old_red_bin, sizeof(red_bin));
			memcpy(blue_bin, old_blue_bin, sizeof(blue_bin));
			for(int cc = 0; cc<32; cc++){
				green_bin[0][cc] = green_bin[0][cc] & clouse_2[0][cc];
				red_bin[0][cc] = red_bin[0][cc] & clouse_2[0][cc];
				blue_bin[0][cc] = blue_bin[0][cc] & clouse_2[0][cc];
			}
		}


		if(d_time>d_b + d_k*4 &&  c == 5){
			c = 6;
			memcpy(green_bin, old_green_bin, sizeof(green_bin));
			memcpy(red_bin, old_red_bin, sizeof(red_bin));
			memcpy(blue_bin, old_blue_bin, sizeof(blue_bin));
			for(int cc = 0; cc<32; cc++){
				green_bin[0][cc] = green_bin[0][cc] & clouse_1[0][cc];
				red_bin[0][cc] = red_bin[0][cc] & clouse_1[0][cc];
				blue_bin[0][cc] = blue_bin[0][cc] & clouse_1[0][cc];
			}
		}

	  	if(d_time>d_b + d_k*5 && c == 6){
			memcpy(green_bin, old_green_bin, sizeof(green_bin));
			memcpy(red_bin, old_red_bin, sizeof(red_bin));
			memcpy(blue_bin, old_blue_bin, sizeof(blue_bin));
			c = 0;
			last_time = time;
	  	}





//	    CDC_Transmit_FS((uint64_t *)red_bin, strlen(red_bin));
//	    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);
//	    HAL_Delay(1000);
//	    HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_RESET);
//	    HAL_Delay(1000);



	  	memcpy(str_us, str_rx, sizeof(str_us));
//	  	str_us[1] = '3';
//	  	str_us[2] = '2';
//	  	str_us[4] = '1';
//	  	str_us[5] = '2';
	  	if(str_us[1]>=48 && str_us[1]<58 && str_us[2]>=48 && str_us[2]<58 && str_us[4]>=48 && str_us[4]<58 && str_us[5]>=48 && str_us[5]<58){
	  		x_usb = (str_us[1]-'0')*10 + (str_us[2]-'0');
	  		y_usb = (str_us[4]-'0')*10 + (str_us[5]-'0');
	  	}
	    sprintf(str_tx,"co: [%ld]\n", y_usb);
	    CDC_Transmit_FS((uint8_t *)str_tx, strlen(str_tx));

	    int center_x = x_usb;
	    int center_y = y_usb;


	    int center_x_bit = center_x - 32;
	    if(center_x_bit>=0){
			for(int eye = 0; eye<10; eye++){
				green_bin[0][center_y + eye] = green_bin[0][center_y + eye] & ~(~eye_green_bin[0][eye]>>abs(center_x_bit));
				red_bin[0][center_y + eye] = red_bin[0][center_y + eye] & ~(~eye_red_bin[0][eye]>>abs(center_x_bit));
				blue_bin[0][center_y + eye] = blue_bin[0][center_y + eye] & ~(~eye_blue_bin[0][eye]>>abs(center_x_bit));
			}
	    }
	    else{
		    for(int eye = 0; eye<10; eye++){
		    	green_bin[0][center_y + eye] = green_bin[0][center_y + eye] & ~(~eye_green_bin[0][eye]<<abs(center_x_bit));
				red_bin[0][center_y + eye] = red_bin[0][center_y + eye] & ~(~eye_red_bin[0][eye]<<abs(center_x_bit));
				blue_bin[0][center_y + eye] = blue_bin[0][center_y + eye] & ~(~eye_blue_bin[0][eye]<<abs(center_x_bit));
		    }
	    }

		  for(int z = 0; z<1; z++){
		  for( int j = 0; j<16;j++){

			  Y= j;

			  set_row(Y);
			  int x = 0;


			  uint64_t one = 1;
			  for (int i = 0; i < 64; i++)
			  {
				  //top
				  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y]>>(63-i)&one));
				  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y]>>(63-i)&one));
				  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y]>>(63-i)&one));
				  //bottom
				  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y+16]>>(63-i)&one));
				  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y+16]>>(63-i)&one));
				  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y+16]>>(63-i)&one));
				  clock();
			  } // for X

			  lat();
			  oe();
		  } // for Y
		  }//z



  }
  /* USER CODE END 3 */

}


void clock(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_SET);
}

void lat(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_SET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
}
void oe(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  for(int d=0;d<0x100;d++);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
}

void set_row(int row){
	  if (row & 0x01) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);

	  if (row & 0x02) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET);

	  if (row & 0x04) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_RESET);

	  if (row & 0x08) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_RESET);
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/


static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3
                           PA4 PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB3 PB4 PB5 PB6
                           PB7 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
